pragma solidity 0.6.9;

import "@openzeppelin/contracts-ethereum-package/contracts/Initializable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/GSN/Context.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/utils/Counters.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC721/ERC721Pausable.sol";


contract EqualityVote is Initializable, ContextUpgradeSafe, AccessControlUpgradeSafe, ERC721PausableUpgradeSafe {
    using Counters for Counters.Counter;
    Counters.Counter private tokenIdTracker;
    
    bytes32 public constant CANDIDATE_ROLE = keccak256("CANDIDATE_ROLE");
    bytes32 public constant VOTER_ROLE = keccak256("VOTER_ROLE");
    
    enum State { PREPARE, STARTED, FINISHED, PAUSED }
    State public state;
    
    mapping(uint256=>address) private tokens;
    
    event onCandidateRegistered(address candidate);
    event onVoterRegistered(address voter, uint256 tokenId);
    event onVoted(address voter, address candidate, uint256 tokenId);
    event onSystemStateChanged(State state);
    
    function initialize() public initializer {
        _initialize(
            "EqualityVote",
            "EVT",
            "https://equality-vote.com/api"
        );
        state = State.PREPARE;
    }
    
    function _initialize(string memory name, string memory symbol, string memory baseURI) internal initializer {
        __Context_init_unchained();
        __AccessControl_init_unchained();
        __ERC165_init_unchained();
        __ERC721_init_unchained(name, symbol);
        __Pausable_init_unchained();
        __ERC721Pausable_init_unchained();
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setBaseURI(baseURI);
    }
    
    function mint() private returns (uint256) {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        uint256 tokenId = tokenIdTracker.current();
        _mint(_msgSender(), tokenId);
        tokenIdTracker.increment();
        return tokenId;
    }
    
    function startVoting() public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        require(state == State.PREPARE, "Permission denied: system is either started or paused.");
        state = State.STARTED;
        emit onSystemStateChanged(state);
    }
    
    function stopVoting() public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        require(state != State.FINISHED, "Permission denied: voting is already finished.");
        _pause();
        state = State.FINISHED;
        emit onSystemStateChanged(state);
    }
    
    function regCandidate(address addr) public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        require(hasRole(CANDIDATE_ROLE, addr) == false, "Permission denied: address owner is already registered as a candidate");
        _setupRole(CANDIDATE_ROLE, addr);
        emit onCandidateRegistered(addr);
    }
    
    function regVoter(address addr) public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        require(hasRole(VOTER_ROLE, addr) == false, "Permission denied: address owner is already registered as a voter");
        _setupRole(VOTER_ROLE, addr);
        uint256 tokenId = mint();
        super.safeTransferFrom(_msgSender(), addr, tokenId);
        emit onVoterRegistered(addr, tokenId);
    }
    
    // For public 
    function vote(address candidate, uint256 tokenId) external {
        require(state == State.STARTED, "Permission denied: voting is already finished.");
        require(hasRole(VOTER_ROLE, _msgSender()), "Permission denied: voter is required");
        require(hasRole(CANDIDATE_ROLE, candidate), "Permission denied: address owner is not a candidate");
        super.safeTransferFrom(_msgSender(), candidate, tokenId);
        emit onVoted(_msgSender(), candidate, tokenId);
    }
 
    // Exceptions   
    function emergencyStop() public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        _pause();
        state = State.PAUSED;
        emit onSystemStateChanged(state);
    }
    
    function systemReset() public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Permission denied: contract admin is required");
        for(uint256 i=0; i<tokenIdTracker.current(); i++) {
            _burn(i);
            delete tokens[i];
        }
        _unpause();
        state = State.PREPARE;
        emit onSystemStateChanged(state);
    }
}